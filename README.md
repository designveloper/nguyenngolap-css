#CSS 1
Intern: Nguyen Ngo Lap

##1. Getting started
\- File naming tips:

- Use lowercase letters
- Don't use spaces and symbols and separate words with an underscore or dash
- Use concise and descriptive names

##2. CSS Core
\- **Reference:**

- Codrops: https://tympanus.net/codrops/css_reference/
- Mozilla Dev Network

\- Class selector:

- Self-defined value
- Can be used multiple times per page

\- ID selector:

- Self-defined value
- Can only be used once per page, and must have unique value

\- Color resourse:

- http://colours.neilorangepeel.com/
- https://coolors.co/
- https://randoma11y.com/#/?_k=wdragy

\- Specificity:

- type < class < id
- Resources:
	- http://cssspecificity.com/
	- https://specificity.keegan.st/

##3. Typography

- **Typography:** The study of the design and use of type for communication
- **Typeface:** A set of fonts, designed with common characteristics and composed of glyphs
- **Font:** Individual files that are part of a typeface
- **Web_Safe fonts:** Fonts that are commonly preinstalled on devices

###Types of typefaces

1. Serif: Have small decorative lines
2. Sans-serif: Have no decorative lines
3. Script: hand-lettered look
4. Decorative: Distinct and ornamental
5. Monospace: Each character uses the same amount of horizontal space

###Using a Font Stack

- Choose similar fonts
- Use commas to separate each option
- Use a generic font family as thelast option
- Always declare generic fonts without quotes

###Web fonts

####Internal source

- Downloaded font files
- Included in project files
- Declare and link to font files using @font-face

```
@font-face {
	font-family: 'Museo Sans';
	src:url(museo-sans.ttf) format('ttf'),
		url(museo-sans.woff) format('woff');
}

body {
	font-family: 'Museo Sans', sans-serif;
}
```

- Different browsers support different font format
- Resources: 
	- https://css-tricks.com/snippets/css/using-font-face/
	- Generate different font types (formats): https://www.fontsquirrel.com/tools/webfont-generator

####External source

- Third-party online service (Typekit, Google Fonts, etc.)
- No need to download
- Link directly to CSS and font files hosted online

###Font size:

- **px:** Default 16px
- **em:** 1em = inherited font-size = default 16px
- **rem:** 1rem = 1em = default 16px. Relative only to the root element (HTML)
- Relative values are calculated based on the nearest ancestor element
- Absolute values are not affected by ancestor elements

##4. Layouts

\- **The box model:** Margin -> Border -> Padding -> Width/Height

\- Clearfix: https://css-tricks.com/snippets/css/clear-fix/

\- The box model fix:
```
html {
	box-sizing: border-box;
}

*, *:before, *:after {
	box-sizing: inherit;
}
```
- Reference: https://www.paulirish.com/2012/box-sizing-border-box-ftw/

---

#CSS 2
Intern: Nguyen Ngo Lap

##1. CSS Selectors
\- Except for class and ID, other attribute selectors follow the format:

```
[attr] { /* selects the matched attribute */ }
[attr=val] { /* selects the matched attribute and value */ }
```

\- Selector:

- Descendant selector: Selects any element that's nested inside (`ancestor parent/child/...`)
- Child selector: Selects the first child element only - the element that follows right after the parent element (`parent > child`)
- Sibling combinators:
	- Adjacent (+): Selects only the following chosen element
	- General (~): Selects all the chosen element that follows
- Grouping multiple selectors with commas: `p1, h2, a, ...  {}`
- Combining multiple class value: `.button.submit.cancel... {}`
- Pseudo-class selector: A keyword, added to a selector with the ":" symbol, used to specify a certain state. E.g.: `a:hover, a:click, a:active, ...`
	- `:first-child`: Selects the first child element of the parent
	- `:last-child`: Selects the last child element of the parent
	- `:first-of-type`: Selects the first child element of its type
	- `:last-of-type`: Selects the last child elemnt of its type
	- `:nth-child`: Selects one or more child elements based on the order within the parent container. Argument:
		- Keyword: `nth-child(odd/even)`
		- Number: `nth-child(3)`
		- Algebraic expression: `nth-child(an + b)` (n starts at 0)
	- `:nth-of-type`: Same as above
- Pseudo-element selector: Selects certain parts of the elements that are not a part of the DOM tree (Creating new fake elements?). E.g.:
	- `:before/:after`: Inserting string content

##2. Layouts

\- Horizontal navigation without list:

- Using inline-block:
	- HTML renders line feed as a white space
	- Reduce font size to 0 to clear the white space
	- Restore font size for the link
- Using float

###Position
\- Used to arrange elements relative to the default page flow or browser viewport. Values: relative, absolute, fixed, static, inherit

- Relative: Elements stay in natural flow
- Absolute: Elements removed from natural flow
- Fixed: Similar to absolute, but always relative to the viewport and stay fixed at one place
- Static: Default. Elements not positioned at all
- Inherit: Inherits value from ancestor element

\- Position is also used with a combination of offset properties: top, right, bottom, left

\- Reference: https://tympanus.net/codrops/css_reference/position/

###Float, display or position?
\- Float:

- Variable and flexible content
- Global or large page structures

\- Display:

- Aligning page components (beware of extra space)
- Aligning elements that need to be center aligned
- Doesn't change the natural page flow

\- Position:

- Positioning elements relative to another element
- Aligning elements outside of the document flow
- Aligning elements to a specific spot

\- Three of these options cannot be used on the same element

###Layers and z-index
\- Natural stacking order: block < float < inline < position

##3. Tips and tools
###Resetting stylesheets
\- Reset stylesheet: Stylesheet containing rules that override all the default browser styles to an un-styled baseline

- Reference: https://meyerweb.com/eric/tools/css/reset/

\- Normalize: Stylesheet containing rules that aim to create consistent default styles, rather than removing them

- Reference: https://necolas.github.io/normalize.css/

###Icon fonts

- Add imagery without using images
- Can be styled with CSS
- Font Awesome: http://fontawesome.io/

###Background property
\- Background image's property: https://www.w3schools.com/cssref/pr_background-image.asp

\- Can use multiple background images (seperate with commas)

###Alpha transparency and gradients
\- Alpha transparency syntax: `rgba(R, G, B, Alpha Transparency (from 0 to 1))`

\- Can use gradient as background: `linear-gradient(color, color)`

\- Overlaying color on a background image:

- Use alpha transparency (need 2 containers)
- Use gradient:
	- `background: linear-gradient(rgba(...), rgba(...)), url(...) ...`

##Responsive and mobile
\- Layouts:

- Static: Fixed
- Adaptive: Only optimized for certain viewport sizes (a set of fixed layouts)
- Liquid (Fluid): Elements take up a percentage of the total size (get squished or stretched when resized)
- Responsive: Adaptive + Liquid

\- Media queries: Specify how a document is presented on different media

\- Breakpoints: The points to decide to change layout style

###Graceful degradation:

- Design for modern browsers first
- Provide fallbacks for unsupported features
- Ensure basic functionality works for older browsers

###Progressive enhancement:

- Focus on content and accessibility first
- Create base-level experience first
- Add advance features for enhancements

###Creating flexible layouts

- Start with fluid CSS before adding responsive CSS
- Make the content fit relative to its container
- Don't rely only on defined devices sizes
- Use percentage-based widths
- Use min/max-widths to add constraints to the layout
- Use percentage-based widths and min/max together

###Media query best practices

- Reference: https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries
- Try not to use too many media queries
- Combine the ones that are close in size
- Try not to get fixated on specific screen sizes
- Add a breakpoint when the design itself needs it
